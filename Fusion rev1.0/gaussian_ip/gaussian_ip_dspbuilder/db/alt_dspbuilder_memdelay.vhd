-- This file is not intended for synthesis, is is present so that simulators
-- see a complete view of the system.

-- You may use the entity declaration from this file as the basis for a
-- component declaration in a VHDL file instantiating this entity.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

entity alt_dspbuilder_memdelay is
	generic (
		RAMTYPE : string := "AUTO";
		WIDTH : positive := 8;
		DELAY : positive := 1
	);
	port (
		output : out std_logic_vector(WIDTH-1 downto 0);
		input : in std_logic_vector(WIDTH-1 downto 0) := (others=>'0');
		aclr : in std_logic := '0';
		user_aclr : in std_logic := '0';
		ena : in std_logic := '0';
		clock : in std_logic := '0'
	);
end entity alt_dspbuilder_memdelay;

architecture rtl of alt_dspbuilder_memdelay is

component alt_dspbuilder_memdelay_GNR55NYJWV is
	generic (
		RAMTYPE : string := "AUTO";
		WIDTH : positive := 1;
		DELAY : positive := 2
	);
	port (
		aclr : in std_logic := '0';
		clock : in std_logic := '0';
		ena : in std_logic := '0';
		input : in std_logic_vector(1-1 downto 0) := (others=>'0');
		output : out std_logic_vector(1-1 downto 0);
		user_aclr : in std_logic := '0'
	);
end component alt_dspbuilder_memdelay_GNR55NYJWV;

component alt_dspbuilder_memdelay_GNT3M75IMA is
	generic (
		RAMTYPE : string := "AUTO";
		WIDTH : positive := 10;
		DELAY : positive := 1
	);
	port (
		aclr : in std_logic := '0';
		clock : in std_logic := '0';
		ena : in std_logic := '0';
		input : in std_logic_vector(10-1 downto 0) := (others=>'0');
		output : out std_logic_vector(10-1 downto 0);
		user_aclr : in std_logic := '0'
	);
end component alt_dspbuilder_memdelay_GNT3M75IMA;

component alt_dspbuilder_memdelay_GNXMJOJMJV is
	generic (
		RAMTYPE : string := "AUTO";
		WIDTH : positive := 8;
		DELAY : positive := 2
	);
	port (
		aclr : in std_logic := '0';
		clock : in std_logic := '0';
		ena : in std_logic := '0';
		input : in std_logic_vector(8-1 downto 0) := (others=>'0');
		output : out std_logic_vector(8-1 downto 0);
		user_aclr : in std_logic := '0'
	);
end component alt_dspbuilder_memdelay_GNXMJOJMJV;

component alt_dspbuilder_memdelay_GN7KC3ZSDB is
	generic (
		RAMTYPE : string := "AUTO";
		WIDTH : positive := 8;
		DELAY : positive := 1
	);
	port (
		aclr : in std_logic := '0';
		clock : in std_logic := '0';
		ena : in std_logic := '0';
		input : in std_logic_vector(8-1 downto 0) := (others=>'0');
		output : out std_logic_vector(8-1 downto 0);
		user_aclr : in std_logic := '0'
	);
end component alt_dspbuilder_memdelay_GN7KC3ZSDB;

component alt_dspbuilder_memdelay_GND4ZY57YF is
	generic (
		RAMTYPE : string := "AUTO";
		WIDTH : positive := 8;
		DELAY : positive := 13
	);
	port (
		aclr : in std_logic := '0';
		clock : in std_logic := '0';
		ena : in std_logic := '0';
		input : in std_logic_vector(8-1 downto 0) := (others=>'0');
		output : out std_logic_vector(8-1 downto 0);
		user_aclr : in std_logic := '0'
	);
end component alt_dspbuilder_memdelay_GND4ZY57YF;

component alt_dspbuilder_memdelay_GNATJSJFWZ is
	generic (
		RAMTYPE : string := "AUTO";
		WIDTH : positive := 10;
		DELAY : positive := 5271
	);
	port (
		aclr : in std_logic := '0';
		clock : in std_logic := '0';
		ena : in std_logic := '0';
		input : in std_logic_vector(10-1 downto 0) := (others=>'0');
		output : out std_logic_vector(10-1 downto 0);
		user_aclr : in std_logic := '0'
	);
end component alt_dspbuilder_memdelay_GNATJSJFWZ;

component alt_dspbuilder_memdelay_GNOSZN6L7S is
	generic (
		RAMTYPE : string := "AUTO";
		WIDTH : positive := 8;
		DELAY : positive := 2608
	);
	port (
		aclr : in std_logic := '0';
		clock : in std_logic := '0';
		ena : in std_logic := '0';
		input : in std_logic_vector(8-1 downto 0) := (others=>'0');
		output : out std_logic_vector(8-1 downto 0);
		user_aclr : in std_logic := '0'
	);
end component alt_dspbuilder_memdelay_GNOSZN6L7S;

component alt_dspbuilder_memdelay_GNGGI56XG4 is
	generic (
		RAMTYPE : string := "AUTO";
		WIDTH : positive := 8;
		DELAY : positive := 664
	);
	port (
		aclr : in std_logic := '0';
		clock : in std_logic := '0';
		ena : in std_logic := '0';
		input : in std_logic_vector(8-1 downto 0) := (others=>'0');
		output : out std_logic_vector(8-1 downto 0);
		user_aclr : in std_logic := '0'
	);
end component alt_dspbuilder_memdelay_GNGGI56XG4;

component alt_dspbuilder_memdelay_GNIL6D42UG is
	generic (
		RAMTYPE : string := "AUTO";
		WIDTH : positive := 10;
		DELAY : positive := 2675
	);
	port (
		aclr : in std_logic := '0';
		clock : in std_logic := '0';
		ena : in std_logic := '0';
		input : in std_logic_vector(10-1 downto 0) := (others=>'0');
		output : out std_logic_vector(10-1 downto 0);
		user_aclr : in std_logic := '0'
	);
end component alt_dspbuilder_memdelay_GNIL6D42UG;

component alt_dspbuilder_memdelay_GNQ44RXMM7 is
	generic (
		RAMTYPE : string := "AUTO";
		WIDTH : positive := 8;
		DELAY : positive := 1320
	);
	port (
		aclr : in std_logic := '0';
		clock : in std_logic := '0';
		ena : in std_logic := '0';
		input : in std_logic_vector(8-1 downto 0) := (others=>'0');
		output : out std_logic_vector(8-1 downto 0);
		user_aclr : in std_logic := '0'
	);
end component alt_dspbuilder_memdelay_GNQ44RXMM7;

begin

alt_dspbuilder_memdelay_GNR55NYJWV_0: if ((RAMTYPE = "AUTO") and (WIDTH = 1) and (DELAY = 2)) generate
	inst_alt_dspbuilder_memdelay_GNR55NYJWV_0: alt_dspbuilder_memdelay_GNR55NYJWV
		generic map(RAMTYPE => "AUTO", WIDTH => 1, DELAY => 2)
		port map(aclr => aclr, clock => clock, ena => ena, input => input, output => output, user_aclr => user_aclr);
end generate;

alt_dspbuilder_memdelay_GNT3M75IMA_1: if ((RAMTYPE = "AUTO") and (WIDTH = 10) and (DELAY = 1)) generate
	inst_alt_dspbuilder_memdelay_GNT3M75IMA_1: alt_dspbuilder_memdelay_GNT3M75IMA
		generic map(RAMTYPE => "AUTO", WIDTH => 10, DELAY => 1)
		port map(aclr => aclr, clock => clock, ena => ena, input => input, output => output, user_aclr => user_aclr);
end generate;

alt_dspbuilder_memdelay_GNXMJOJMJV_2: if ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 2)) generate
	inst_alt_dspbuilder_memdelay_GNXMJOJMJV_2: alt_dspbuilder_memdelay_GNXMJOJMJV
		generic map(RAMTYPE => "AUTO", WIDTH => 8, DELAY => 2)
		port map(aclr => aclr, clock => clock, ena => ena, input => input, output => output, user_aclr => user_aclr);
end generate;

alt_dspbuilder_memdelay_GN7KC3ZSDB_3: if ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 1)) generate
	inst_alt_dspbuilder_memdelay_GN7KC3ZSDB_3: alt_dspbuilder_memdelay_GN7KC3ZSDB
		generic map(RAMTYPE => "AUTO", WIDTH => 8, DELAY => 1)
		port map(aclr => aclr, clock => clock, ena => ena, input => input, output => output, user_aclr => user_aclr);
end generate;

alt_dspbuilder_memdelay_GND4ZY57YF_4: if ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 13)) generate
	inst_alt_dspbuilder_memdelay_GND4ZY57YF_4: alt_dspbuilder_memdelay_GND4ZY57YF
		generic map(RAMTYPE => "AUTO", WIDTH => 8, DELAY => 13)
		port map(aclr => aclr, clock => clock, ena => ena, input => input, output => output, user_aclr => user_aclr);
end generate;

alt_dspbuilder_memdelay_GNATJSJFWZ_5: if ((RAMTYPE = "AUTO") and (WIDTH = 10) and (DELAY = 5271)) generate
	inst_alt_dspbuilder_memdelay_GNATJSJFWZ_5: alt_dspbuilder_memdelay_GNATJSJFWZ
		generic map(RAMTYPE => "AUTO", WIDTH => 10, DELAY => 5271)
		port map(aclr => aclr, clock => clock, ena => ena, input => input, output => output, user_aclr => user_aclr);
end generate;

alt_dspbuilder_memdelay_GNOSZN6L7S_6: if ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 2608)) generate
	inst_alt_dspbuilder_memdelay_GNOSZN6L7S_6: alt_dspbuilder_memdelay_GNOSZN6L7S
		generic map(RAMTYPE => "AUTO", WIDTH => 8, DELAY => 2608)
		port map(aclr => aclr, clock => clock, ena => ena, input => input, output => output, user_aclr => user_aclr);
end generate;

alt_dspbuilder_memdelay_GNGGI56XG4_7: if ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 664)) generate
	inst_alt_dspbuilder_memdelay_GNGGI56XG4_7: alt_dspbuilder_memdelay_GNGGI56XG4
		generic map(RAMTYPE => "AUTO", WIDTH => 8, DELAY => 664)
		port map(aclr => aclr, clock => clock, ena => ena, input => input, output => output, user_aclr => user_aclr);
end generate;

alt_dspbuilder_memdelay_GNIL6D42UG_8: if ((RAMTYPE = "AUTO") and (WIDTH = 10) and (DELAY = 2675)) generate
	inst_alt_dspbuilder_memdelay_GNIL6D42UG_8: alt_dspbuilder_memdelay_GNIL6D42UG
		generic map(RAMTYPE => "AUTO", WIDTH => 10, DELAY => 2675)
		port map(aclr => aclr, clock => clock, ena => ena, input => input, output => output, user_aclr => user_aclr);
end generate;

alt_dspbuilder_memdelay_GNQ44RXMM7_9: if ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 1320)) generate
	inst_alt_dspbuilder_memdelay_GNQ44RXMM7_9: alt_dspbuilder_memdelay_GNQ44RXMM7
		generic map(RAMTYPE => "AUTO", WIDTH => 8, DELAY => 1320)
		port map(aclr => aclr, clock => clock, ena => ena, input => input, output => output, user_aclr => user_aclr);
end generate;

assert not (((RAMTYPE = "AUTO") and (WIDTH = 1) and (DELAY = 2)) or ((RAMTYPE = "AUTO") and (WIDTH = 10) and (DELAY = 1)) or ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 2)) or ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 1)) or ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 13)) or ((RAMTYPE = "AUTO") and (WIDTH = 10) and (DELAY = 5271)) or ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 2608)) or ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 664)) or ((RAMTYPE = "AUTO") and (WIDTH = 10) and (DELAY = 2675)) or ((RAMTYPE = "AUTO") and (WIDTH = 8) and (DELAY = 1320)))
	report "Please run generate again" severity error;

end architecture rtl;

