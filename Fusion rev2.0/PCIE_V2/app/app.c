#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#define PAGE_SIZE						4096
#define MMAP_SIZE						PAGE_SIZE*1024

/*Array size to write and read*/
#define MAX_SIZE_IN						640*480
#define MAX_SIZE_OUT					640*480

/*Operator code for ioctl*/
#define IOCTL_START_VDMA_READ 			0
#define IOCTL_START_VDMA1_READ 			1
#define IOCTL_START_VDMA2_READ 			2
#define IOCTL_START_VDMA_WRITE	  		3
#define IOCTL_CHANGE_TO_FRAME1	  		4
#define IOCTL_CHANGE_TO_FRAME2	  		5

int main() {
	FILE *fi1, *fi2;
	int dev; 
	
	dev = open("/dev/de2i150_altera", O_RDWR);
	if(dev < 0)
    {
        perror("Open device file failed");
        return -1;
    }
	
	unsigned int *mem = NULL;
	int i;
	
	mem = mmap(NULL, MMAP_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, dev, 0);
	if (mem == MAP_FAILED)
    {
        perror("mmap operation failed");
        return -1;
    }
	
	fi1 = fopen("video3_in.bin", "rb");
	fi2 = fopen("video4_in.bin", "rb");

	ioctl(dev, IOCTL_START_VDMA_READ, 0);
	ioctl(dev, IOCTL_START_VDMA1_READ, 0);
	ioctl(dev, IOCTL_START_VDMA2_READ, 0);
	ioctl(dev, IOCTL_START_VDMA_WRITE, 0);

	while(1)
	{
		fseek(fi1, 0, SEEK_SET);
		fseek(fi2, 0, SEEK_SET);
		while(!feof(fi1)) 
		{
			ioctl(dev, IOCTL_CHANGE_TO_FRAME1, 0);
			fread(mem, sizeof(unsigned char), MAX_SIZE_IN, fi1);
			write(dev, NULL, MAX_SIZE_IN);
			
			ioctl(dev, IOCTL_CHANGE_TO_FRAME2, 0);
			fread(mem+(MMAP_SIZE/2)/4, sizeof(unsigned char), MAX_SIZE_IN, fi2);
			write(dev, NULL, MAX_SIZE_IN);

			usleep(40000);
		}
	}
    
	fclose(fi1);
	fclose(fi2);
	close(dev);
	
	return 0;
}

